#!/bin/bash

apt-get install lsb-release figlet python-apt -y

mkdir /etc/update-motd.d/

cp 00-header 10-sysinfo 20-updates 99-footer /etc/update-motd.d/

chmod +x /etc/update-motd.d/*

rm /etc/motd

ln -s /var/run/motd /etc/motd

cd / 

rm -r /root/motd